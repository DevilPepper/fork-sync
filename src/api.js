const axios = require('axios');
const readline = require('readline');
const fs = require('fs');

const api = "https://gitlab.com/api/v4"
const headers = {
        "PRIVATE-TOKEN": process.env.PRIVATE_TOKEN,
        "Content-Type": "application/json"
};

const artifacts_dir = ".ci_status";

const createDir = () => {
    if (!fs.existsSync(artifacts_dir)) {
        fs.mkdirSync(artifacts_dir);
    }
};

module.exports.gitlab = axios.create({baseURL: api, headers: headers});
module.exports.stdin = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
})

module.exports.appendFile = (file, contents) => {
    createDir();
    fs.appendFileSync(`${artifacts_dir}/${file}`, contents);
}
