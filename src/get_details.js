const {gitlab, stdin} = require('./api.js');

stdin.on("line", (id) => {
    gitlab.get(`/projects/${id}`)
         .then((res) => {
             let details = res.data;
             gitlab.get(`/projects/${id}/variables/BRANCHES_TO_SYNC`)
                   .then(variable => fixOutput(details, variable.data.value))
                   .catch(err => {
                       if (!process.env.BRANCHES_TO_SYNC) {
                           gitlab.get(`/projects/${details.forked_from_project.id}/repository/branches`)
                                 .then(branches => {
                                     output(details, branches.data.map(b => b.name));
                                 })
                       } else {
                           fixOutput(details, process.env.BRANCHES_TO_SYNC);
                       }
                   })
    });
});

function fixOutput(details, branches) {
    output(details, branches.split(","));
}


function output(details, branches) {
    console.log(JSON.stringify({details: details, branches: branches}));
}
