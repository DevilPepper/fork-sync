const {gitlab, stdin} = require('./api.js');

stdin.on("line", (id) => {

    gitlab.get(`/users/${id}/projects`)
         .then((res) => {
             let data = res.data;
             data.forEach((project) => {
                 console.error(`Project name: ${project.name}`);
                 console.error(`Git Url: ${project.ssh_url_to_repo}`);
                 if(project.forked_from_project !== null) {
                     console.log(project.id);
                 }
             })
    })
});
